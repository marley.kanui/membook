class CreateMems < ActiveRecord::Migration[5.2]
  def change
    create_table :mems do |t|
      t.string :mem_name 
      t.string :mood
      t.text :thoughts
      t.string :location
      t.timestamps
    end
  end
end
